# CouchDB TLS
Start a blank couchdb container and mount TLS configuration and certificates in volumes. 

Keep in mind that this kind of mount has its limits in user and group permissions. The container is usually started with
a couchdb user with id 5984 (representing the default port as well). The mounted directories will be changed in their
access permissions from `$HOST_USER:$HOST_GROUP` to `couchdb:couchdb` (or in IDs `5984:5984`).

## Config by docker-compose.yml
Within the [docker-compose.yml](docker-compose.yml) we have to configure
* admin user name and password as `env` variables
* at least 2 volume mounts for
  * enabling couchdb TLS config with [local.ini](certs/local.ini) &rarr; `./certs/local.ini:/opt/couchdb/etc/local.d/local.ini`
  * providing private key and certificate at location specified within `local.ini` &rarr; `./certs/:/opt/couchdb/certs/`
* (optional) port forwarding to host
* (optional) volume mount for couchdb persistence

## Managing TLS Certificates
### Create Private and Public Keys
To create example certificates, i.e. private and public keys, we can use the following commands (not production ready!):
~~~bash
# assuming pwd is project's root directory:

# go to certs directory
cd certs

# create private key 
openssl genrsa > host_key.pem

# create certificate signed by private key
openssl req -new -x509 -key host_key.pem -out host_cert.pem -days 365
~~~

### Add Private Key to Java truststore
To enable http clients to securely communicate with the couchdb we have to provide the public key within a truststore:
~~~bash
# change certificate format from pem to der for Java keystore
openssl x509 -outform der -in host_cert.pem -out host_cert.der

# add certificate to truststore (if truststore doesn't exist yet, it is created)
keytool -import -alias couch_db_cert -keystore the_truststore.p12 -file host_cert.der
~~~

### Try Certificate Creation with config file `config/req.conf`
See https://support.citrix.com/article/CTX135602 for an example of 'adding several CNs' so to speak.
